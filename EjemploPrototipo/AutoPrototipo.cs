﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploPrototipo
{
    public abstract class AutoPrototipo
    {
        //Atributos de la clase abstracta
        private string Color;
        private string Modelo;

        //Set de los respectivos atributos
        public void setColor(string Color)
        {
            this.Color = Color;
        }

        public void setModelo(string Modelo)
        {
            this.Modelo = Modelo;
        }

        //Get de los respectivos atributos
        public string getColor()
        {
            return Color;
        }

        public string getModelo()
        {
            return Modelo;
        }

        //Método abstracto que nos devuelve el prototipo de un auto
        public abstract AutoPrototipo Clonar();

        //Método abstracto para obtener información del auto
        public abstract string VerAuto();
    }
}
