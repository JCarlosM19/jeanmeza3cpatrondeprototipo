﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploPrototipo
{
    //Establecer la herencia de AutoPrototipo
    public class AlfaRomeoPrototipo : AutoPrototipo
    {
        //Emplear todos los métodos provenientes de la clase abstracta
        public override AutoPrototipo Clonar()
        {
            //El método MemberwiseClone devuelve una copia superficial del objeto de tipo AlfaRomeo
            return (AlfaRomeoPrototipo)this.MemberwiseClone();
        }

        //Muestra la información del auto Alfa Romeo
        public override string VerAuto()
        {
            return "Alfa Romeo: " + getModelo() + " | Color: " + getColor();
        }
    }
}
