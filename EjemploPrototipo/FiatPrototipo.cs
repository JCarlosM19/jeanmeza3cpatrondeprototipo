﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploPrototipo
{
    //Establecer la herencia de AutoPrototipo
    public class FiatPrototipo : AutoPrototipo
    {
        //Emplear todos los métodos provenientes de la clase abstracta
        public override AutoPrototipo Clonar()
        {
            //El método MemberwiseClone devuelve una copia superficial del objeto de tipo FiatPrototipo
            return (FiatPrototipo)this.MemberwiseClone();
        }

        //Muestra la información del auto Fiat
        public override string VerAuto()
        {
            return "Fiat: " + getModelo() + " | Color: " + getColor();
        }
    }
}
