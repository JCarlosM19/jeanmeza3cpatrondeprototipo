﻿using System;

namespace EjemploPrototipo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creación de instancias prototípicas
            //Creación de un objeto de tipo AutoPrototipo con el constructor FiatPrototipo 
            AutoPrototipo PrototipoFiat = new FiatPrototipo();
            //Creacion de un objeto de tipo AutoPrototipo con el constructor DSPrototipo 
            AutoPrototipo PrototipoDS = new DSPrototipo();
            //Creacion de un objeto de tipo AutoPrototipo con el constructor AlfaRomeoPrototipo
            AutoPrototipo PrototipoAlfaRomeo = new AlfaRomeoPrototipo();

            /*Creación de un objeto llamado FiatPalio de tipo AutoPrototipo y se le asignan las caracteristicas 
             correspondiente a través del método clonar.*/
            AutoPrototipo FiatPalio = PrototipoFiat.Clonar();
            //Ingresando valores de color al objeto
            FiatPalio.setColor("Negro");
            //Ingresando valores de modelo al objeto
            FiatPalio.setModelo("Palio Fire");
            //Mostrar en la consola los datos asignados
            Console.WriteLine(FiatPalio.VerAuto());

            AutoPrototipo FiatUno = FiatPalio.Clonar();
            FiatUno.setColor("Blanco");
            FiatUno.setModelo("Uno SRC");
            Console.WriteLine(FiatUno.VerAuto());

            AutoPrototipo DS3 = PrototipoDS.Clonar();
            DS3.setColor("Blanco");
            DS3.setModelo("3 Chic");
            Console.WriteLine(DS3.VerAuto());

            AutoPrototipo DS4 = PrototipoDS.Clonar();
            DS4.setColor("Negro");
            DS4.setModelo("4 Sport");
            Console.WriteLine(DS4.VerAuto());

            AutoPrototipo AlfaRomeo145 = PrototipoAlfaRomeo.Clonar();
            AlfaRomeo145.setColor("Blanco");
            AlfaRomeo145.setModelo("145");
            Console.WriteLine(AlfaRomeo145.VerAuto());

            /*Creación de un objeto llamado AlfaRomeo146 de tipo AutoPrototipo y se le asignan las caracteristicas 
             correspondiente a través del método clonar.*/
            AutoPrototipo AlfaRomeo146 = PrototipoAlfaRomeo.Clonar();
            //Ingresando valores de color al objeto
            AlfaRomeo146.setColor("Negro");
            //Ingresando valores de modelo al objeto
            AlfaRomeo146.setModelo("146");
            //Mostrar en la consola los datos asignados
            Console.WriteLine(AlfaRomeo146.VerAuto());
        }
    }
}
